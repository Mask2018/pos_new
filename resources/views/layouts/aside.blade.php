 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
            <img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
            <p> {{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">


        <li class="active">
        <a href="#" >
               <i class="fa fa-dashboard"></i> <span class="" style="text-transform:Capitalize;"> Dashboard</span>
                    <span class="pull-right-container">
                </span>
            </a>
            </li>
            <li class="treeview menu-open active hide ">

            <a href="#">
              <i class="fa fa-users"></i> <span>Customers</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          <ul class="treeview-menu">
            <li class="">
            <a href="#">
                <i class="fa fa-users"></i> <span>Client</span>
                    <span class="pull-right-container">
                </span>
            </a>
            </li>
            <li class="">

                <a href="#"><i class="fa fa-circle-o"></i> Events</a>
            </li>


            <li class=" hide ">
                <a href="#"><i class="fa fa-circle-o"></i> Venue</a></li>
          </ul>
        </li>
        <li class="">
            <a href="#">
                <i class="fa fa-users"></i> <span>Client</span>
                    <span class="pull-right-container">
                </span>
            </a>
            </li>

              <li class="">
              <a href="#">
                  <i class="fa fa-users"></i> <span>Staff</span>
                      <span class="pull-right-container">
                  </span>
              </a>
              </li>
            <li class="">
            <a href="#">
                <i class="fa fa-users"></i> <span>Client</span>
                    <span class="pull-right-container">
                </span>
            </a>
            <li class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>Schedule</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          <ul class="treeview-menu">

            <li class="treeview">
              <a href="#"><i class="fa fa-calendar-o"></i> Events
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="">
                  <a href="#"><i class="fa fa-circle-o"></i> Add Events Details </a></li>
                  <li class="">
                  <a href="#"><i class="fa fa-circle-o"></i> View Events Schedule</a>
                </li>
              </ul>
            </li>


            <li class="">
            <a href="#"><i class="fa fa-circle-o"></i> Venue</a>
            </li>
          </ul>
        </li>
            <li class="treeview">
              <a href="">
                  <i class="fa fa-calendar"></i> <span>Schedule</span>
                  <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i>
                  </span>


              </a>
              <ul class="treeview-menu">
                <li class="">

                  <a href=""><i class="fa fa-circle-o"></i> Add Guarding Details </a>
                </li>
                  <li class="">
                  <a href=""><i class="fa fa-circle-o"></i> View Guarding Schedule</a>
                </li>
              </ul>
            </li>

         <!--- Dashboard Check --->





            <li class="">

            <a href="#">
                <i class="fa fa-money"></i> <span>Pay Roll</span>
                    <span class="pull-right-container">
                </span>
            </a>
            </li>

            <li class="">
            <a href="#">
                <i class="fa fa-clock-o"></i> <span>Attendance</span>
                    <span class="pull-right-container">
                </span>
            </a>
            </li>

            <li class="">
            <a href="#">
                <i class="fa  fa-file-excel-o"></i> <span>Reports</span>
            </a>
            </li>
        </ul>
        </section>
        <!-- /.sidebar -->
    </aside>